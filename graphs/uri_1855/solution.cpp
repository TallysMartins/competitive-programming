/*
----------------------------------------------------------------------------
  Name: Tallys Gustavo Martins
  Email: tallys@ime.usp.br
  Degree in Software Engineering from University of Brasília
  URL: https://www.urionlinejudge.com.br/judge/pt/problems/view/1855
  LEVEL: 2
  Timelimit: 1
  Subject: Elementary Graph Algorithms
  Algorithm: DFS

  Time.now('mm/yyyy')
  >> 10/2017

  Problem: Count if a map achieves the treasure
  Solution: Simple DFS to navigate trough the grid and verify cycles
----------------------------------------------------------------------------
*/

#include <bits/stdc++.h>

using namespace std;

#define MAX_X 102
#define MAX_Y 102


char grid[MAX_X][MAX_Y];
int visited[MAX_X][MAX_Y];
int mystack[MAX_X][MAX_Y];
char direction;

pair <int, int> get_adj(int x, int y) {
  pair <int, int> new_coord;
  if(direction == '<')
    new_coord = pair<int, int> (x, y-1);
  if(direction == '^')
    new_coord = pair<int, int> (x+1,y);
  if(direction == '>')
    new_coord = pair<int, int> (x,y+1);
  if(direction == 'V')
    new_coord = pair<int, int> (x-1,y);

  if(grid[new_coord.first][new_coord.second] != '.') {
    direction = grid[new_coord.first][new_coord.second];
  }

  return new_coord;

}

char dfs(int x, int y) {
  visited[x][y] = 1; 

  if(grid[x][y] == '*')
    return '*';
    
  int adj_x, adj_y;
  tie(adj_x, adj_y) = get_adj(x,y);

  if(not visited[adj_x][adj_y]){ 
    return dfs(adj_x, adj_y);
  }else{
    return '!';
  }
}

int main() {

  int X; //The widht of the grid
  int Y; //The height of the grid
  char element; //The current element read from the input

  cin >> X >> Y;
  memset(mystack, -1, sizeof(mystack));
  memset(mystack[0], 0, sizeof(mystack[0]));
  memset(mystack[Y+1], 0, sizeof(mystack[0]));
  memset(visited, 0, sizeof(visited));
  
  for(int i=1; i<=Y; i++) {
    for(int k=1; k<=X; k++) {
      cin >> element;  
      grid[i][k] = element;
      mystack[i][0] = 0;
      mystack[i][X+1] = 0;
    }
  }

  direction = grid[1][1];
  cout << dfs(1,1) << endl;
      
  return 0;
}
