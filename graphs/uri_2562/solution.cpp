/*
----------------------------------------------------------------------------
  Name: Tallys Gustavo Martins
  Email: tallys@ime.usp.br
  Degree in Software Engineering from University of Brasília
  URL: https://www.urionlinejudge.com.br/judge/pt/problems/view/2562
  LEVEL: 2
  Timelimit: 1
  Subject: Elementary Graph Algorithms
  Algorithm: BFS

  Time.now('mm/yyyy')
  >> 10/2017

  Problem: Count which component of a graph has least nodes
  Solution: Simple BFS to transverse the graph and count the number of vertexes
  in the component
----------------------------------------------------------------------------
*/

#include <bits/stdc++.h>

using namespace std;

#define N 1000
#define M N*(N-1)/2

int visited[N]
int min_pokemons;
vector<int> adj[N];

void bfs(int s) {
  queue<int> Q;
  visited[s] = 1;
  Q.emplace(s);
  int u;
  int pokemons_counter = 1;

  while(not Q.empty()) {
    int u = Q.front(); Q.pop();

    visited[u] = 1;
    for(auto v : adj[u]) {
      if(not visited[v]){
        Q.emplace(v);
        visited[v] = 1;

        days_counter = max(days_counter, u_d + 1);
      }
    }
  }
}

int main() {
  int T; // Number of test cases
  int A; // Number of lines in the grid
  int B; // Number of columns in the grid
  int X; // X position of the first tree to be cured
  int Y; // Y position of the first tree to be cured
  int element; // Current element being read from the input

  cin >> T;

  while(T--) {
    cin >> A >> B;

    memset(grid, 0 , sizeof(grid));
    memset(grid[0], -1, sizeof(grid[0]));
    memset(grid[A+1], -1, sizeof(grid[A+1]));
    memset(visited, 0, sizeof(visited));
    days_counter = 0;

    for(int i=0; i<=A+1; ++i) {
      grid[i][0] = -1;
      grid[i][B+1] = -1;
    }

    for(int i=1; i<=A; ++i){
      for(int j=1; j<=B; ++j){
          cin >> element;
          grid[i][j] = element;
          if(element == 1) {
            remaining_sick_trees++;
          }
      }
    }
    cin >> X >> Y;
    bfs(X,Y);
    cout << days_counter << endl;
  }

  return 0;
}
